# vue_lovexhjTo520

#### 项目介绍
#### 想为你做一幅画，以心为笔，以情为墨，以爱你为内容，以余生为落笔。
#### 耳中所听恍若你呢喃，心之所向是指你为南，目之所及除你之外尽是荒野。

原项目：lovexhjTo520,地址：        
https://gitee.com/nutssss/lovexhjTo520.

改造为vue版本，增添点界面效果。当前项目仅为前端部分，后端golang部分已剥离。提交的跨域接口可用，仅供测试。       
效果预览：https://homeapp.top/love

#### 相关技术
javascript vue html5 css golang



#### 使用说明

1.  本地双击打开index.html即可预览
2.  也可以提交到服务器上运行，修改为对应目录


### 更多素材说明
参考这个界面，补充了相关ui：     

https://h5.eqxiu.com/s/wjgA1keD?eqrcode=1       
 